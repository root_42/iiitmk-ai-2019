
Generated on 2019-08-21 05:35:10.615278

Assignments Submitted: [![coverage report](https://gitlab.com/gitcourses/iiitmk-ai-2019/badges/Master/coverage.svg)](https://gitlab.com/gitcourses/iiitmk-ai-2019/commits/Master)

| name                  | HackerRank | Assignment | email                        | hackerrank link                            |
| --------------------- | ---------- | ---------- | ---------------------------- | -------------------------------------------|
| AbittaVR              | 0          | 0          | abitta.da3@iiitmk.ac.in      | None                                       |
| Akhil-K-K             | 0          | 0          | akhil.mi3@iiitmk.ac.in       | None                                       |
| akhil-siby            | 0          | 0          | akhil.da3@iiitmk.ac.in       | None                                       |
| Alida-Baby            | 0          | 0          | alda.mi3@iiitmk.ac.in        | None                                       |
| Anandha-Krishnan-H    | 6          | 1          | anandha.mi3@iiitmk.ac.in     | https://www.hackerrank.com/AnandhaKrishnanH|
| anandhu-bhaskar       | 0          | 0          | anandhu.da3@iiitmk.ac.in     | None                                       |
| anitta-augustine      | 0          | 0          | anitta.da3@iiitmk.ac.in      | None                                       |
| Anju-Vinod            | 0          | 0          | anju.da3@iiitmk.ac.in        | None                                       |
| ann-mary              | 2          | 0          | ann.mi3@iiitmk.ac.in         | https://www.hackerrank.com/ann_mi3         |
| anu-elizabath-shibu   | 0          | 0          | anu.da3@iiitmk.ac.in         | https://www.hackerrank.com/anu_da3         |
| anushka-srivastava    | 14         | 1          | anushka.da3@iiitmk.ac.in     | https://www.hackerrank.com/anushka_da3     |
| arjoonn               | 16         | 0          | arjoonn.msccsc5@iiitmk.ac.in | https://www.hackerrank.com/arjoonn         |
| Arvind                | 0          | 0          | arvind.mi3@iiitmk.ac.in      | None                                       |
| brittosabu            | 10         | 1          | britto.da3@iiitmk.ac.in      | https://www.hackerrank.com/brittosabu07    |
| chinju-murali         | 2          | 0          | chinju.mi3@iiitmk.ac.in      | https://www.hackerrank.com/chinjumurali    |
| Dhanesh               | 0          | 0          | dhanesh.mi3@iiitmk.ac.in     | None                                       |
| Giridhar_K            | 8          | 0          | giridhar.da3@iiitmk.ac.in    | https://www.hackerrank.com/giridhar_da3    |
| gokul-p               | 0          | 0          | gokul.da3@iiitmk.ac.in       | None                                       |
| hashim-abdulla        | 0          | 0          | hashim.mi3@iiitmk.ac.in      | None                                       |
| Hima-Santhosh         | 0          | 0          | hima.da3@iiitmk.ac.in        | None                                       |
| jose-vincent          | 0          | 0          | jose.da3@iiitmk.ac.in        | https://www.hackerrank.com/jose_vincent    |
| Lipsa                 | 6          | 0          | lipsa.da3@iiitmk.ac.in       | https://www.hackerrank.com/lipsajohny      |
| malavika-james        | 0          | 0          | malavika.da3@iiitmk.ac.in    | https://www.hackerrank.com/malavika_da3    |
| megha-ghosh           | 0          | 0          | megha.mi3@iiitmk.ac.in       | https://www.hackerrank.com/megha_f1        |
| meghana-muraleedharan | 0          | 0          | meghana.da3@iiitmk.ac.in     | None                                       |
| mobin-m               | 0          | 0          | mobin.mi3@iiitmk.ac.in       | https://www.hackerrank.com/the_DEMYSTIFier |
| nasim-sulaiman        | 0          | 0          | nasim.mi3@iiitmk.ac.in       | None                                       |
| navya-jose            | 0          | 0          | navya.mi3@iiitmk.ac.in       | None                                       |
| nithin-g              | 0          | 0          | nithin.da3@iiitmk.ac.in      | https://www.hackerrank.com/nithin_da3      |
| nitish                | 10         | 1          | nitish.mi3@iiitmk.ac.in      | https://www.hackerrank.com/niteshmichael   |
| OliviTJ               | 0          | 0          | None                         | None                                       |
| pallavi-pannu         | 12         | 1          | pallavi.da3@iiitmk.ac.in     | https://www.hackerrank.com/pallavi_da3     |
| prabhatika            | 16         | 1          | prabhatika.vij@gmail.com     | https://www.hackerrank.com/prabhatika_vij  |
| PRAVEEN-PRATIK        | 0          | 0          | praveen.da3@iiitmk.ac.in     | None                                       |
| princ3                | 6          | 1          | prince.mi3@iiitmk.ac.in      | https://www.hackerrank.com/logontoprince   |
| sagnik-mukherjee      | 2          | 1          | sagnik.mi3@iiitmk.ac.in      | https://www.hackerrank.com/sagnik_mi3      |
| sandeep_ma            | 0          | 0          | sandeep.da3@iiitmk.ac.in     | None                                       |
| Sanjumariam           | 2          | 1          | sanju.mi3@iiitmk.ac.in       | https://www.hackerrank.com/sanju_mi3       |
| sidharth-manmadhan    | 0          | 0          | sidharth.mi3@iiitmk.ac.in    | None                                       |
| Sreehari-P-V          | 0          | 1          | sreehari.mras@gmail.com      | https://www.hackerrank.com/sreehari_mi3    |
| sukesh_s              | 2          | 1          | sukesh.mi3@iiitmk.ac.in      | https://www.hackerrank.com/sukesh_mi3      |
| Tathagata-Ghosh       | 16         | 0          | tathagata.da3@iiitmk.ac.in   | https://www.hackerrank.com/tathagata_da3   |
| ummarshaik            | 0          | 0          | ummar.da3@iiitmk.ac.in       | None                                       |
| VaibhawKumar          | 0          | 0          | vaibhaw.da3@iiitmk.ac.in     | None                                       |
| vinu-abraham          | 0          | 1          | vinuabraham.mi3@iiitmk.ac.in | None                                       |
| vinu-alex             | 0          | 0          | vinu.mi3@iiitmk.ac.in        | None                                       |
