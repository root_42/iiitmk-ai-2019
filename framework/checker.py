import os
import sys
from pathlib import Path
import sys

sols = Path("../../framework/solutions")
sys.path.append(str(sols.resolve()))
# ------------------------------
submissions = {}
for pyfile in Path(".").glob("*.py"):
    print(pyfile)
    with open(pyfile, "r") as fl:
        exec(fl.read(), submissions)
# ------------------------------
assignments_done = 0
for fn in submissions.keys():
    if fn.startswith("assignment"):
        print("testing ", fn)
        test = {}
        with open(sols / f"{fn}_solution.py", "r") as fl:
            exec(fl.read(), test)
        try:
            test["test"](submissions[fn])
        except AssertionError as e:
            print(f"Your solution for {fn} fails for input:")
            print(e)
            sys.exit(1)
        else:
            assignments_done += 1
total = 0
for fl in os.listdir("../../assignments"):
    with open(f"../../assignments/{fl}", "r") as fl:
        code = fl.read()
    scope = {}
    exec(code, scope)
    total += sum([1 for name in scope.keys() if name.startswith("assignment")])
fraction = assignments_done / total
print(f"AssignmentsDone: {fraction*100}")
